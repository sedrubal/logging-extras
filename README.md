# Logging Utils CLI

Utilities to use python logging system as output of command line interfaces.

## License

[(c) 2023 Sebastian Endres - MIT](./LICENSE)
